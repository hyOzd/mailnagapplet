This is a Cinnamon applet for showing new mail notifications. You
should install and configure
[mailnag](https://github.com/pulb/mailnag/) daemon for this applet to
work.

## Installing
- make sure you've installed and configured mailnag
- copy `mailnagapplet@ozderya.net` folder to `~/.local/share/cinnamon/applets/`
- add applet to your panel (Settings->Applets or right click to panel)

#### Archlinux users
- you can install from [AUR](https://aur.archlinux.org/packages/cinnamon-applet-mailnag-git/)

## Features
- displays number of mails
- lists unread mails on click
- mark all mail read on middle click
- displays notifications on mail arrival

## Screenshots
![tooltip](./img/mailnag_tooltip.png)
![tooltip](./img/mailnag_menu.png)
